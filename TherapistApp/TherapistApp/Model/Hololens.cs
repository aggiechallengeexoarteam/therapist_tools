﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TherapistApp.Connectivity;
using Microsoft.Tools.WindowsDevicePortal;
using static Microsoft.Tools.WindowsDevicePortal.DevicePortal;

namespace TherapistApp.Model
{
    public class Hololens
    {
        DeviceMonitor monitor;

        protected Hololens() { }

        public static Hololens CreateHololens()
        {
            return new Hololens();
        }

        public string IP { get; set; }
        public string Username { get; set; }
        public string Pass { get; set; }
        public bool Connected { get; set; }

        public async Task Connect()
        {
            ConnectOptions options = new ConnectOptions(IP,
                                                        "Fred",
                                                        Username,
                                                        Pass);
            monitor = await DeviceMonitor.ConnectToDeviceAsync(options);        
        }

        public async Task Launch(string packageName)
        {
            if(!Connected)
            {
                await Connect();
            }
            AppPackages packages = await monitor.GetInstalledApplicationsAsync();
            foreach (PackageInfo package in packages.Packages)
            {
                if (package.Name == packageName)
                {
                    await monitor.LaunchApplicationAsync(package.AppId, package.Name);
                    return;
                }
            }
        }

        public async Task Kill(string packageName)
        {
            if (!Connected)
            {
                await Connect();
            }
            await monitor.TerminateAllApplicationsAsync();
            //RunningProcesses processes = await monitor.GetRunningProcessesAsync();
            //foreach (DeviceProcessInfo process in processes.Processes)
            //{
            //    if (process.AppName == packageName)
            //        await monitor.TerminateApplicationAsync(packageName);
            //}
        }
    }
}

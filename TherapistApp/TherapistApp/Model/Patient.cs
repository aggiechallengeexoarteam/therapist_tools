﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TherapistApp.Model
{
    public class Patient
    {
        public static Patient CreateNewPatient()
        {
            return new Patient();
        }

        protected Patient() { }

        public static Patient CreatePatient(string firstName,
                                            string lastName,
                                            string sex,
                                            int age,
                                            double armLength,
                                            double ipd)
        {
            return new Patient
            {
                FirstName = firstName,
                LastName = lastName,
                Sex = sex,
                Age = age,
                ArmLength = armLength,
                IPD = ipd
            };
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Sex { get; set; }
        public int Age { get; set; }
        public double ArmLength { get; set; }
        public double IPD { get; set; }
    }
}

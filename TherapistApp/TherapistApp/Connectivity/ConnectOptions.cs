﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.

namespace TherapistApp.Connectivity
{
    /// <summary>
    /// Object encapsulating the options for connecting to a device.
    /// </summary>
    public class ConnectOptions
    {
        /// <summary>
        /// The IP address to be used for the connection.
        /// </summary>
        public string Address = "127.0.0.1:10080";

        /// <summary>
        /// Optional name for the device.
        /// </summary>
        public string Name = "Fred";

        /// <summary>
        /// The password associated with the user account.
        /// </summary>
        public string Password = "Connectivity";

        /// <summary>
        /// The name of the user.
        /// </summary>
        public string UserName = "Connectivity";

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectOptions" /> class.
        /// </summary>
        public ConnectOptions() : this(
            string.Empty,
            string.Empty,
            string.Empty,
            string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectOptions" /> class.
        /// </summary>
        /// <param name="address">The address to be used for the connection.</param>
        /// <param name="name">Optional, local name for the device.</param>
        /// <param name="userName">The name to use when connecting to the device.</param>
        /// <param name="password">The password to use when connecting to the device.</param>
        public ConnectOptions(
            string address,
            string name,
            string userName,
            string password)
        {
            this.Address = address;
            this.Name = name;
            this.UserName = userName;
            this.Password = password;
        }
    }
}

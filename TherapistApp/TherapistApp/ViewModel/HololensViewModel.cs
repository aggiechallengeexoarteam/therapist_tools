﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TherapistApp.Model;

namespace TherapistApp.ViewModel
{
    public class HololensViewModel : ViewModelBase
    {
        readonly Hololens _hololens;

        public HololensViewModel()
        {
            _hololens = Hololens.CreateHololens();
        }

        #region Model Properties
        public string IP
        {
            get { return _hololens.IP; }
            set
            {
                if (value == _hololens.IP)
                    return;

                _hololens.IP = value;
                base.OnPropertyChanged("IP");
            }
        }

        public string Username
        {
            get { return _hololens.Username; }
            set
            {
                if (value == _hololens.Username)
                    return;

                _hololens.Username = value;
                base.OnPropertyChanged("Username");
            }
        }

        public string Pass
        {

            get { return _hololens.Pass; }
            set
            {
                if (value == _hololens.Pass)
                    return;

                _hololens.Pass = value;
                base.OnPropertyChanged("Pass");
            }
        }
        #endregion

        public Hololens Hololens
        {
            get
            {
                return _hololens;
            }
        }
    }
}

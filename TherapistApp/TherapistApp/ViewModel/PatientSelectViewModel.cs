﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TherapistApp.DataAccess;
using TherapistApp.Model;

namespace TherapistApp.ViewModel
{
    public class PatientSelectViewModel : ViewModelBase
    {
        readonly PatientRepository _patientRepository;
        readonly Hololens _hololens;
        RelayCommand _openCommand;
        RelayCommand _newCommand;

        public PatientSelectViewModel(PatientRepository patientRepository, Hololens hololens)
        {
            if (patientRepository == null)
                throw new ArgumentNullException("patientRepository");
            if(hololens == null)
            {
                throw new ArgumentNullException("hololens");
            }

            base.DisplayName = "Patient Select";

            _hololens = hololens;
            _patientRepository = patientRepository;
            _patientRepository.PatientAdded += this.OnPatientAddedToRepository;
            this.CreateAllPatients();
        }

        void CreateAllPatients()
        {
            List<PatientViewModel> all =
                (from patient in _patientRepository.GetPatients()
                 select new PatientViewModel(patient, _patientRepository, _hololens)).ToList();

            foreach (PatientViewModel pvm in all)
                pvm.PropertyChanged += this.OnPatientViewModelPropertyChanged;

            this.AllPatients = new ObservableCollection<PatientViewModel>(all);
            this.AllPatients.CollectionChanged += this.OnCollectionChanged;
        }

        public ObservableCollection<PatientViewModel> AllPatients { get; private set; }

        public IEnumerable<PatientViewModel> SelectedPatients
        {
            get
            {
                return AllPatients.Where(pvm => pvm.IsSelected ? true : false);
            }
        }

        protected override void OnDispose()
        {
            foreach (PatientViewModel pvm in this.AllPatients)
                pvm.Dispose();

            this.AllPatients.Clear();
            this.AllPatients.CollectionChanged -= this.OnCollectionChanged;

            _patientRepository.PatientAdded -= this.OnPatientAddedToRepository;

        }

        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
                foreach (PatientViewModel pvm in e.NewItems)
                    pvm.PropertyChanged += this.OnPatientViewModelPropertyChanged;

            if (e.OldItems != null && e.OldItems.Count != 0)
                foreach (PatientViewModel pvm in e.OldItems)
                    pvm.PropertyChanged -= this.OnPatientViewModelPropertyChanged;
        }

        void OnPatientViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        void OnPatientAddedToRepository(object sender, PatientAddedEventArgs e)
        {
            var viewModel = new PatientViewModel(e.NewPatient, _patientRepository, _hololens);
            this.AllPatients.Add(viewModel);
        }

        public ICommand OpenProfiles
        {
            get
            { 
                if (_openCommand == null)
                    _openCommand = new RelayCommand(param => this.OnRequestOpenProfiles());

                return _openCommand;
            }
        }

        public ICommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                    _newCommand = new RelayCommand(param => this.OnRequestNewProfile());

                return _newCommand;
            }
        }

        public event EventHandler RequestOpenProfiles;
        public event EventHandler RequestNewProfile;

        void OnRequestOpenProfiles()
        {
            RequestOpenProfiles?.Invoke(this, EventArgs.Empty);
        }

        void OnRequestNewProfile()
        {
            RequestNewProfile?.Invoke(this, EventArgs.Empty);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using TherapistApp.DataAccess;
using TherapistApp.Model;
using TherapistApp.Properties;

namespace TherapistApp.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        readonly PatientRepository _patientRepository;
        ObservableCollection<ViewModelBase> _workspaces;
        HololensViewModel _hololensViewModel;
        Hololens _hololens;

        public MainWindowViewModel(string patientDataPath)
        {
            base.DisplayName = "Nicole";
            _patientRepository = new PatientRepository(patientDataPath);
            _hololensViewModel = new HololensViewModel();
            _hololens = _hololensViewModel.Hololens;
            ShowAllPatients();
        }

        public ObservableCollection<ViewModelBase> Workspaces
        {
            get
            {
                if(_workspaces == null)
                {
                    _workspaces = new ObservableCollection<ViewModelBase>();
                    _workspaces.CollectionChanged += this.OnWorkspacesChanged;
                }
                return _workspaces;
            }
        }

        public HololensViewModel HololensViewModel
        {
            get
            {
                if(_hololensViewModel == null)
                {
                    _hololensViewModel = new HololensViewModel();
                    _hololens = _hololensViewModel.Hololens;
                }
                return _hololensViewModel;
            }
        }

        void OnWorkspacesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
                foreach (ViewModelBase vm in e.NewItems)
                {
                    PatientViewModel pvm = vm as PatientViewModel;
                    if (pvm != null)
                    {
                        pvm.RequestClose += this.OnPatientViewModelRequestClose;
                    }
                }

            if (e.OldItems != null && e.OldItems.Count != 0)
                foreach (ViewModelBase vm in e.OldItems)
                {
                    PatientViewModel pvm = vm as PatientViewModel;
                    if (pvm != null)
                    {
                        pvm.RequestClose -= this.OnPatientViewModelRequestClose;
                    }
                }
        }

        void OnPatientSelectViewModelRequestOpen(object sender, EventArgs e)
        {
            PatientSelectViewModel psvm = sender as PatientSelectViewModel;
            foreach(PatientViewModel pvm in psvm.SelectedPatients)
            {
                Workspaces.Add(pvm);
                SetActiveWorkspace(pvm);
            }
        }

        void OnPatientSelectViewModelRequestNewProfile(object sender, EventArgs e)
        {
            this.CreateNewPatient();
            _patientRepository.SavePatients(Resources.dataPath);
        }

        void OnPatientViewModelRequestClose(object sender, EventArgs e)
        {
            PatientViewModel pvm = sender as PatientViewModel;
            pvm.Dispose();
            this.Workspaces.Remove(pvm);
        }

        void CreateNewPatient()
        {
            Patient newPatient = Patient.CreateNewPatient();
            PatientViewModel pvm = new PatientViewModel(newPatient, _patientRepository, _hololens);
            this.Workspaces.Add(pvm);
            this.SetActiveWorkspace(pvm);
            
        }

        void ShowAllPatients()
        {
            PatientSelectViewModel psvm =
                this.Workspaces.FirstOrDefault(vm => vm is PatientSelectViewModel)
                as PatientSelectViewModel;

            if(psvm == null)
            {
                psvm = new PatientSelectViewModel(_patientRepository, _hololens);
                psvm.RequestOpenProfiles += OnPatientSelectViewModelRequestOpen;
                psvm.RequestNewProfile += OnPatientSelectViewModelRequestNewProfile;
                this.Workspaces.Add(psvm);
            }

            this.SetActiveWorkspace(psvm);
        }

        void SetActiveWorkspace(ViewModelBase workspace)
        {
            Debug.Assert(this.Workspaces.Contains(workspace));
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.Workspaces);
            if (collectionView != null)
                collectionView.MoveCurrentTo(workspace);
        }

        protected override void OnDispose()
        {
            _patientRepository.SavePatients(Resources.dataPath);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TherapistApp.DataAccess;
using TherapistApp.Model;
using TherapistApp.Properties;

namespace TherapistApp.ViewModel
{
    public class PatientViewModel : ViewModelBase
    {
        readonly Patient _patient;
        readonly PatientRepository _patientRepository;
        readonly Hololens _hololens;
        bool _isSelected;
        string _selectedGame;
        RelayCommand _saveCommand;
        RelayCommand _launchCommand;
        RelayCommand _killCommand;
        RelayCommand _closeCommand;

        public PatientViewModel(Patient patient, PatientRepository patientRepository, Hololens hololens)
        {
            if (patient == null)
                throw new ArgumentNullException("patient");
            if (patientRepository == null)
                throw new ArgumentNullException("patientRepository");
            if (hololens == null)
                throw new ArgumentNullException("hololens");

            _patient = patient;
            _patientRepository = patientRepository;
            _hololens = hololens;
        }

        #region Patient Properties

        public string FirstName
        {
            get { return _patient.FirstName; }
            set
            {
                if (value == _patient.FirstName)
                    return;

                _patient.FirstName = value;
                base.OnPropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get { return _patient.LastName; }
            set
            {
                if (value == _patient.LastName)
                    return;

                _patient.LastName = value;
                base.OnPropertyChanged("LastName");
            }
        }

        public string Sex
        {
            get { return _patient.Sex; }
            set
            {
                if (value == _patient.Sex)
                    return;

                _patient.Sex = value;
                base.OnPropertyChanged("Sex");
            }
        }

        public int Age
        {
            get { return _patient.Age; }
            set
            {
                if (value == _patient.Age)
                    return;

                _patient.Age = value;
                base.OnPropertyChanged("Age");
            }
        }

        public double ArmLength
        {
            get { return _patient.ArmLength; }
            set
            {
                if (value == _patient.ArmLength)
                    return;

                _patient.ArmLength = value;
                base.OnPropertyChanged("ArmLength");
            }
        }

        public double IPD
        {
            get { return _patient.IPD; }
            set
            {
                if (value == _patient.IPD)
                    return;

                _patient.IPD = value;
                base.OnPropertyChanged("IPD");
            }
        }

        #endregion

        #region Presentation Properties
        public override string DisplayName
        {
            get
            {
                if(this.IsNewPatient)
                {
                    return "New Patient";
                }
                else
                {
                    return String.Format("{0}, {1}", _patient.LastName, _patient.FirstName);
                }
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value == _isSelected)
                    return;

                _isSelected = value;
                base.OnPropertyChanged("IsSelected");
            }
        }

        public string SelectedGame
        {
            get
            {
                if (_selectedGame == null)
                    _selectedGame = "";
                return _selectedGame;
            }
            set
            {
                if (value == _selectedGame)
                    return;

                _selectedGame = value;
                base.OnPropertyChanged("SelectedGame");
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                if(_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(param => this.Save());
                }
                return _saveCommand;
            }
        }

        public ICommand LaunchCommand
        {
            get
            {
                if (_launchCommand == null)
                    _launchCommand = new RelayCommand(param => this.Launch());
                return _launchCommand;
            }
        }

        public ICommand KillCommand
        {
            get
            {
                if (_killCommand == null)
                    _killCommand = new RelayCommand(param => this.Kill());
                return _killCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                    _closeCommand = new RelayCommand(param => this.OnRequestClose());

                return _closeCommand;
            }
        }

        #endregion

        public void Save()
        {
            if (this.IsNewPatient)
            {
                _patientRepository.AddPatient(_patient);
            }

            _patientRepository.SavePatients(Resources.dataPath);
            base.OnPropertyChanged("DisplayName");
        }

        public async void Launch()
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Wait;
            try
            {              
                string toLaunch = _selectedGame.Split(':')[1].Substring(1);
                await _hololens.Launch(toLaunch);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK);
            }
            Mouse.OverrideCursor = null;
        }

        public async void Kill()
        {
            try
            {
                string toKill = _selectedGame.Split(':')[1].Substring(1);
                await _hololens.Kill(toKill);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK);
            }
        }

        bool IsNewPatient
        {
            get { return !_patientRepository.ContainsPatient(_patient); }
        }

        public event EventHandler RequestClose;

        void OnRequestClose()
        {
            this.RequestClose?.Invoke(this, EventArgs.Empty);
        }
        
    }
}

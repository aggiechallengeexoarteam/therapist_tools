﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using TherapistApp.Model;

namespace TherapistApp.DataAccess
{
    public class PatientRepository
    {
        readonly List<Patient> _patients;

        public PatientRepository(string patientDataPath)
        {
            _patients = LoadPatients(patientDataPath);
        }

        public event EventHandler<PatientAddedEventArgs> PatientAdded;

        public void AddPatient(Patient patient)
        {
            if (patient == null)
                throw new ArgumentNullException("patient");

            if(!_patients.Contains(patient))
            {
                _patients.Add(patient);

                if (this.PatientAdded != null)
                    this.PatientAdded(this, new PatientAddedEventArgs(patient));
            }
        }

        public bool ContainsPatient(Patient patient)
        {
            if (patient == null)
                throw new ArgumentNullException("patient");

            return _patients.Contains(patient);
        }

        public List<Patient> GetPatients()
        {
            return new List<Patient>(_patients);
        }

        static List<Patient> LoadPatients(string patientDataPath)
        {
            List<Patient> patients = new List<Patient>();
            XmlDocument doc = new XmlDocument();
            doc.Load(patientDataPath);
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                patients.Add(Patient.CreatePatient(node.Attributes["firstName"]?.InnerText,
                                                    node.Attributes["lastName"].InnerText,
                                                    node.Attributes["sex"]?.InnerText,
                                                    Convert.ToInt32(node.Attributes["age"]?.InnerText),
                                                    Convert.ToDouble(node.Attributes["armLength"]?.InnerText),
                                                    Convert.ToDouble(node.Attributes["ipd"]?.InnerText)));
            }
            
            return patients;
        }

        public void SavePatients(string patientDataPath)
        {
            
            using (XmlWriter writer = XmlWriter.Create(patientDataPath))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Patients");

                foreach (Patient patient in GetPatients())
                {
                    writer.WriteStartElement("Patient");
                    writer.WriteAttributeString("firstName", patient.FirstName);
                    writer.WriteAttributeString("lastName", patient.LastName);
                    writer.WriteAttributeString("sex", patient.Sex);
                    writer.WriteAttributeString("age", patient.Age.ToString());

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    }
}

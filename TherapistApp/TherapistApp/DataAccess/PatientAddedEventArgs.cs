﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TherapistApp.Model;

namespace TherapistApp.DataAccess
{
    public class PatientAddedEventArgs : EventArgs
    {
        public PatientAddedEventArgs(Patient newPatient)
        {
            this.NewPatient = newPatient;
        }

        public Patient NewPatient { get; private set; }
    }
}

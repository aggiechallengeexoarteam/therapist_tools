﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using TherapistApp.View;
using TherapistApp.DataAccess;
using TherapistApp.ViewModel;

namespace TherapistApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        
protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MainWindow window = new MainWindow();
            string dataPath = "../../Data/patients.xml";
            var mainWindowViewModel = new MainWindowViewModel(dataPath);
            window.DataContext = mainWindowViewModel;
            window.Show();

        }
        
        
    }
}
